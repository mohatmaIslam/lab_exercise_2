#include <iostream>
#include <math.h>
using namespace std;

int main()
{
    //declaring variables
    double celsius, fahrenheit, radius, circumference, areaCircle, areaRectangle, areaTriangle;
    int option;
    const float PI = 3.14159;

    do{
    cout << endl;
    //display menu
    cout << "MENU" << endl;
    cout << "--------------------------------------------" << endl;
    cout << "1. Convert Celsius to Fahrenheit" << endl;
    cout << "2. Convert Fahrenheit to Celsius" << endl;
    cout << "3. Calculate circumference of a circle" << endl;
    cout << "4. Calculate area of a circle" << endl;
    cout << "5. Area of Rectangle" << endl;
    cout << "6. Area of Triangle (Heron's Formula)" << endl;
    cout << "7. Volume of Cylinder" << endl;
    cout << "8. Volume of Cone" << endl;
    cout << "9. Quit Program" << endl;

    //select option
    cout << "\nEnter option: ";
    cin >> option;
    while(option<1 || option>9){
        cout << "Invalid input! Please enter again." << endl;
        cout << endl;
        cout << "Enter option: ";
        cin >> option;
    }

    //switch option
    switch(option){

    case 1: ///completed
            //prompt user input
            cout << "Enter Celsius value to convert to Fahrenheit: ";
            cin >> celsius;
            //calculation formula
            fahrenheit = (celsius * 9.0) / 5.0 + 32;
            //print value
            cout << "\nFahrenheit value is: " << fahrenheit << endl;
            cout << endl;
            break;

    case 2: ///completed
            //prompt user input
            cout << "Enter Fahrenheit value to convert to Celsius: ";
            cin >> fahrenheit;
            //calculation formula
            celsius = (fahrenheit - 32) * 5 / 9;
            //print value
            cout << "\nCelsius value is: " << celsius << endl;
            cout << endl;

            break;


    case 3: ///completed
            //prompt user input
            cout << "Enter the radius of the Circle: ";
            cin >> radius;
            //validate radius value
            while(radius<=1){
                cout << "Input invalid! Radius must be greater than 1." << endl;
                cout << "Enter the radius of the Circle: ";
                cin >> radius;
            }
            //calculation formula
            circumference = 2 * PI * radius;
            //print value
            cout << "Circumference of circle: " << circumference << endl;

            break;

    case 4: ///completed
            //prompt user input
            cout << "Enter the radius of the Circle: ";
            cin >> radius;
            //validate radius value
            while(radius<=1){
                cout << "Input invalid! Radius must be greater than 1." << endl;
                cout << "Enter the radius of the Circle: ";
                cin >> radius;
            }
            //calculation formula
            areaCircle = PI * radius * radius;
            //print value
            cout << "Area of circle: " << areaCircle << endl;

            break;

    case 5: ///completed
            //declare variables
            double length, width, height;
            //prompt user input for length
            cout << "Enter length: ";
            cin >> length;
            while(length<=1){
                cout << "Invalid input! Length should be more than 1." << endl;
                cout << "Enter length: ";
                cin >> length;
            }
            //prompt user input for width
            cout << "Enter width: ";
            cin >> width;
            while(width<=1){
                cout << "Invalid input! Width should be more than 1." << endl;
                cout << "Enter width: ";
                cin >> width;
            }
            //prompt user input for height
            cout << "Enter height: ";
            cin >> height;
            while(height<=1){
                cout << "Invalid input! Height should be more than 1." << endl;
                cout << "Enter height: ";
                cin >> height;
            }
            //calculation formula
            areaRectangle = length*width*height;
            //print value
            cout << "Area of Rectangle: " << areaRectangle << endl;

            break;

     case 6: ///completed
            //declare variables
            double side1, side2, side3, area, s;

            //prompt user for triangle values
            cout << "Enter length of 1st side of the triangle : ";
            cin >> side1;
            while(side1<=1){
                cout << "Invalid input! Input should be greater than 1." << endl;
                cout << "Enter length of 1st side of the triangle : ";
                cin >> side1;
            }

            cout << "Enter length of 2nd side of the triangle : ";
            cin >> side2;
            while(side2<=1){
                cout << "Invalid input! Input should be greater than 1." << endl;
                cout << "Enter length of 2nd side of the triangle : ";
                cin >> side2;
            }

            cout << "Enter length of 3rd side of the triangle : ";
            cin >> side3;
            while(side3<=1){
                cout << "Invalid input! Input should be greater than 1." << endl;
                cout << "Enter length of 3rd side of the triangle : ";
                cin >> side3;
            }

            //calculation formulas
            s = (side1+side2+side3)/2;

            areaTriangle = sqrt(s*(s-side1)*(s-side2)*(s-side3));

            //print value
            cout << " The area of the triangle is : " << areaTriangle << endl;

            break;

    case 7: ///completed

            //declaring variables
            int rad1,hgt;
            float volcy;

            //prompt user for cylinder values
            cout << "Enter radius of the Cylinder : ";
            cin >> rad1;
            while(rad1<=1){
                cout << "Invalid input! Radius should be more than 1." << endl;
                cout << "Enter radius: ";
                cin >> rad1;
            }

            cout << "Enter height of the Cylinder : ";
            cin >> hgt;
            while(hgt<=1){
                cout << "Invalid input! Radius should be more than 1." << endl;
                cout << "Enter height: ";
                cin >> hgt;
            }

            //calculation formula
            volcy=(3.14*rad1*rad1*hgt);
            //print value
            cout << "The volume of a cylinder is : " << volcy << endl;
            cout << endl;

            break;

    case 8: ///completed
        {
            //Declares variables
            float r, h, v;
            float pi = 3.14159;

            //prompt user for cone's values
            cout << "Input cone's radius: ";
            cin >> r;
            while(r<=1){
                cout << "Invalid input! Radius should be more than 1." << endl;
                cout << "Enter radius: ";
                cin >> r;
            }

            cout << "Input cone's height: ";
            cin >>  h;
            while(h<=1){
                cout << "Invalid input! Height should be more than 1." << endl;
                cout << "Enter height: ";
                cin >> h;
            }

            //calculation formula
            v = (1.0/3.0) * pi * (r*r) * h;
            //print value
            cout << "The volume of the cone is\n\n " << v << "\n";

            break;
        }

    case 9: ///completed
        {
            system("Exit");
            break;
        }


    }

    }while(option!=9);

return 0;
}
